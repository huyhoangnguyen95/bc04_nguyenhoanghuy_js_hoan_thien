
function getScore(trTag) {
    var tdList = trTag.querySelectorAll("td");
    return tdList[3].innerText * 1;
}

function getName(trTag) {
    var tdList = trTag.querySelectorAll("td");
    return tdList[2].innerText;
}

var trList = document.querySelectorAll("#tblBody tr");
// console.log('trList: ', trList);
var trMax = trList[0];
var trMin = trList[0];

// yc1
for (var i = 0; i < trList.length; i++) {
    var currentTr = trList[i];
    var currentScore = getScore(currentTr);
    var maxScore = getScore(trMax);
    if (currentScore > maxScore) {
        trMax = currentTr;
        // maxScore = currentScore;
    }
}
document.getElementById("svGioiNhat").innerHTML = ` ${getName(trMax)} - ${getScore(trMax)} `;
// console.log('maxScore: ', maxScore);
// console.log('trMax: ', trMax);

// yc2
for (var i = 0; i < trList.length; i++) {
    var currentTr = trList[i];
    var currentScore = getScore(currentTr);
    var minScore = getScore(trMin);
    if(currentScore < minScore){
        trMin = currentTr;
    }    
}
document.getElementById("svYeuNhat").innerHTML = ` ${getName(trMin)} - ${getScore(trMin)} `;
// console.log('trMin: ', trMin);

// yc3
var arrSvGioi = Array.from(trList).filter(function(item){
    return getScore(item) > 8;
});
document.getElementById("soSVGioi").innerHTML = ` ${arrSvGioi.length}`;
// console.log('arrSvGioi: ', arrSvGioi);


// yc4 
var contentHTML = "";
var arrDssvTren5 = Array.from(trList).filter(function(item){
    return getScore(item) > 5;
});
// console.log('arrDssvTren5: ', arrDssvTren5);
arrDssvTren5.forEach(function(item){
    contentHTML += ` <p> ${getScore(item)} - ${getName(item)} </p> `;
})

document.getElementById("dsDiemHon5").innerHTML = ` ${(contentHTML)} `;


// yc5


var numArr = Array.from(trList).map(function(item){
    return getScore(item);
});
document.getElementById("dtbTang").innerHTML = ` ${(numArr.sort())} `;



